# Artiseum RFQ Service

## Prerequisite

Launch a local instance of [Platform 6](https://documentation.amalto.com/platform6/master/).

## How to run the demo?

### Build the user interface

Go in the root directory and install the global dependencies:

```console
$ npm install
```

Go in the [client's directory](./clients) and install its dependencies:

```console
$ cd client
$ npm install
```

Compile the TypeScript source files to generate the compiled bundle file `client/build/ServiceConfiguration.bundle.js`:

```console
$ npm run build
```

You can also use the watch mode to generate a new bundle file after every change made to the source files. The command is then: `npm run build:watch`.

### Run the server of the service

The server in the demo is built using the framework [Express](https://expressjs.com/).
To launch it, from the root directory:

Install the dependencies:

```console
$ npm install
```

Build and run the server:

```console
$ npm run build
$ npm run start
```

You can also use the watch mode to generate a new bundle file after every change made to the server's code:

```console
$ npm run build:watch
```

Simultaneously, in another shell, run the following command line to automatically restart the server after every build:

```console
$ npm run server:watch
```

It should launch a server on the port `8000` then deploy the service __arteseum.rfq__ on Platform 6.

An entry menu _RFQ_ should appear in the menu of the _Portal_.

When you click on it, it should make a request to the endpoint `GET /api/${VERSION}/arteseum.rfq/portal`.
This endpoint returns the client's bundled JavaScript file `ServiceConfiguration.bundle.js` that you built [at the previous step](#build-the-user-interface).

The _Portal_ will use this response to display the user interface of the service.

![RFQ main tab](images/rfq_list.png)

![RFQ creation form](images/rfq_form.png)

## How does the demo work?

When you click on the RFQ service, you'll see the main data grid filled with dummy RFQs.

These values are retrieved from a script, _ListPrograms_, executed at the service initialization with the endpoint [`GET /api/${VERSION}/arteseum.rfq/list`](src/application.ts#lines-24:37).

__ListPrograms.groovy__

```groovy
import groovy.json.JsonOutput

def programs = []

def program1 = ['name': 'RFQ 1', 'limit': 10]
programs << program1

def program2 = ['name': 'RFQ 2', 'limit': 20]
programs << program2

pipeline.put('programs', JsonOutput.toJson(programs))
```

Once you submit the RFQ creation form, you make a request to the endpoint [`POST /api/${VERSION}/arteseum.rfq/submit`](src/application.ts#lines-38:62).

The endpoint will ask the Platform 6 instance to execute the _CreateMessageInfo_ script and it will pass the RFQ program data in the common message's headers.

The following script will create a new _MessageInfo_ with the RFQ program data.
You need to add it in the Platform 6 instance Scripts service.

__CreateMessageInfo.groovy__

```groovy
log.debug 'Executing create MessageInfo data from RFQ program'

def name = pipeline.get('name')
def limit = pipeline.get('limit')
def startDate = pipeline.get('startDate')
def expiryDate = pipeline.get('expiryDate')
def interest = pipeline.get('interest')
def fee = pipeline.get('fee')
def overdue = pipeline.get('overdue')
def suppliers = pipeline.get('suppliers')
def lenders = pipeline.get('lenders')
def lsps = pipeline.get('lsps')

def xml = """<MessageInfo>
   <Id>${name}</Id>
   <BusinessDocName>FTPInvoice</BusinessDocName>
   <BusinessDocNumber>SC-0001654</BusinessDocNumber>
   <Sender>TESTSCRIPT</Sender>
   <Endpoint>APPROVEME</Endpoint>
   <FinalRecipient>055122568</FinalRecipient>
   <CurrentDocumentFormat>PIDX</CurrentDocumentFormat>
   <CurrentDocumentURI></CurrentDocumentURI>
   <CurrentDocumentContentType>text/xml; charset=utf-8</CurrentDocumentContentType>
   <SourceDocumentFormat>CSV</SourceDocumentFormat>
   <SourceDocumentURI></SourceDocumentURI>
   <SourceDocumentContentType>text/plain</SourceDocumentContentType>
   <CreationDate></CreationDate>
   <LastStatusCode>Pending Review</LastStatusCode>
   <LastStatusMessage/>
   <LastStatusDate/>
    <KeyValue>
      <Key>Limit</Key>
      <Value>${limit}</Value>
    </KeyValue>
    <KeyValue>
      <Key>Start date</Key>
      <Value>${startDate}</Value>
    </KeyValue>
    <KeyValue>
      <Key>Expiry date</Key>
      <Value>${expiryDate}</Value>
    </KeyValue>
    <KeyValue>
      <Key>Interest</Key>
      <Value>${interest}</Value>
    </KeyValue>
    <KeyValue>
      <Key>Fee</Key>
      <Value>${fee}</Value>
    </KeyValue>
    <KeyValue>
      <Key>Overdue</Key>
      <Value>${overdue}</Value>
    </KeyValue>
    <KeyValue>
      <Key>Suppliers</Key>
      <Value>${suppliers}</Value>
    </KeyValue>
    <KeyValue>
      <Key>Lenders</Key>
      <Value>${lenders}</Value>
    </KeyValue>
    <KeyValue>
      <Key>LSPs</Key>
      <Value>${lsps}</Value>
    </KeyValue>
</MessageInfo>
"""

log.debug 'MessageInfo: ' + xml

def ipk = message.buildPK('B2BOX', 'MessageInfo', name)

log.debug 'ipk: ' + ipk

message.project(xml, 'MessageInfo', ipk)
```
