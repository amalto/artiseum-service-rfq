import * as moment from 'moment'

const DAY_FORMAT = 'YYYY-MM-DD'

export function formatDate (date?: string) {
	return moment(date).format(DAY_FORMAT)
}

export function formatTodayDateTime() {
	return moment().format('LLL')
}

export function substractDate(date, count, step) {
	return moment(date).subtract(count, step).format(DAY_FORMAT)
}

export function addDate(date, count, step: string) {
	return moment(date).add(count, step).format(DAY_FORMAT);
}

export interface RFQ {
	name: string
	limit?: number
	startDate?: string
	expiryDate?: string
	interest?: number
	fee?: number
	overdue?: number
}
