import * as React from 'react'
import * as b2portal from '@amalto/platform6-ui'

import ButtonsBar from '@amalto/buttons-bar'
import DateInput from '@amalto/date-input'
import MultiSelect from '@amalto/multi-select'
import TextInput from '@amalto/text-input'

import { addDate, formatTodayDateTime, RFQ, substractDate } from './utils'

const today = getTodayDate()

const suppliers = [
    { value: "ACME suppliers", label: "ACME suppliers" },
    { value: "Indonesian Palm Co", label: "Indonesian Palm Co" }
]

const lenders = [
	{ value: "American Mutual Fund", label: "American Mutual Fund" },
	{ value: "Goldman Sachs", label: "Goldman Sachs" },
	{ value: "American Pension Funds", label: "American Pension Funds" },
]

const lsps = [
	{ value: "Maersk", label: "Maersk" },
	{ value: "Vasi Shipping Ltd", label: "Vasi Shipping Ltd" }
]

declare namespace CreationForm {
	interface Props extends React.Props<CreationForm>, b2portal.forms.FormProps<any, any, any> {
		onSubmit: (values?: RFQ, clearForm?: () => void) => void
	}

	interface State {
		suppliers: string[]
		lenders: string[]
		lsps: string[]
	}
}

class CreationForm extends React.Component<CreationForm.Props, CreationForm.State> {
	state = {
		suppliers: [],
		lenders: [],
		lsps: []
	}

	processSubmitValues (data) {
		const { state } = this

		const newRFQ = {
			...data,
			name: `RFQ - ${formatTodayDateTime()}`,
			suppliers: state.suppliers,
			lenders: state.lenders,
			lsps: state.lsps
		}

		this.props.onSubmit(newRFQ)
	}

	render () {
		const { state } = this

		const min = substractDate(today, 1, 'month')
		const max = addDate(today, 1, 'month')

		return <form
			onSubmit={this.props.handleSubmit(data => this.processSubmitValues(data))}>
			<h2 className="mgb-20">Create a new RFQ program</h2>

			<div className="row">
				<TextInput
					name="limit"
					type="number"
					label="Limit"
					containerClass="col-xs-12 col-md-4 col-lg-4" />

				<DateInput
					name="startDate"
					minDate={min}
					maxDate={max}
					label="Start date"
					containerClass="col-xs-12 col-md-4 col-lg-4" />

				<DateInput
					name="expiryDate"
					minDate={min}
					maxDate={max}
					label="Expiry date"
					containerClass="col-xs-12 col-md-4 col-lg-4" />
			</div>

			<div className="row">
				<TextInput
					name="interest"
					type="number"
					label="Annual interest rate (%)"
					containerClass="col-xs-12 col-md-4 col-lg-4" />

				<TextInput
					name="fee"
					type="number"
					label="Annual fee percentage (%)"
					containerClass="col-xs-12 col-md-4 col-lg-4" />

				<TextInput
					name="overdue"
					type="number"
					label="Annual overdue rate (%)"
					containerClass="col-xs-12 col-md-4 col-lg-4" />
			</div>

            <div className="row">
                <MultiSelect
					name="suppliers"
                    label="List of suppliers"
                    options={suppliers}
                    multiple
                    value={state.suppliers}
                    handleChange={this.handleChangeSuppliers}
                    containerClass="col-xs-12 col-md-4 col-lg-4"
                    inputClass="info-color" />

				<MultiSelect
					name="lenders"
					label="List of lenders"
					options={lenders}
					multiple
					value={state.lenders}
					handleChange={this.handleChangeLenders}
					containerClass="col-xs-12 col-md-4 col-lg-4"
					inputClass="info-color" />

				<MultiSelect
					name="lsps"
					label="List of LSPs"
					options={lsps}
					multiple
					value={state.lsps}
					handleChange={this.handleChangeLsps}
					containerClass="col-xs-12 col-md-4 col-lg-4"
					inputClass="info-color" />
			</div>

			{this.renderButtonsBar()}
		</form>
	}

	private handleChangeSuppliers = event => this.setState({ suppliers: event.target.value })

	private handleChangeLenders = event => this.setState({ lenders: event.target.value })

	private handleChangeLsps = event => this.setState({ lsps: event.target.value })

	private renderButtonsBar = (): JSX.Element => {
		const btnSave = {
			btns: [{
				cssClass: 'btn btn-primary btn-trans bottom-margin',
				iconClass: 'fa fa-fw fa-save',
				text: 'Submit',
				type: 'submit',
				disabled: false
			}]
		}

		const btnReset = {
			btns: [{
				clickAction: () => this.clearForm(),
				cssClass: 'btn btn-default btn-trans bottom-margin',
				iconClass: 'fa fa-fw fa-undo',
				text: 'Reset',
				disabled: false
			}]
		}

		return <ButtonsBar btnGroups={[btnSave, btnReset]} />
	}

	private clearForm = () => this.props.reset()
}

function getTodayDate (): string {
	return new Date().toISOString()
}

export default CreationForm
