import * as React from 'react'
import * as b2portal from '@amalto/platform6-ui'
const { DataGrid, forms, reactRedux, webStorage } = b2portal

import ButtonsBar from '@amalto/buttons-bar'
import DataLine from '@amalto/data-line'
import * as Interfaces from '@amalto/typings'
import Tab from '@amalto/tab'
import Tabs from '@amalto/tabs'

import { RFQ, formatDate } from './utils'
import CreationFormContent from './CreationForm'

const MAIN_TAB_ID = 'rfq_list'

const username = webStorage.user.userEmail

declare namespace ServiceConfiguration {
	interface Props extends ServiceConfiguration, b2portal.DynamicComponent.CustomProps {}

	interface State {
		tabs?: Tab[]
		selectedTabId?: string
		rfqs: RFQ[];
	}
}

class ServiceConfiguration extends React.Component<ServiceConfiguration.Props, ServiceConfiguration.State> {
	state = {
		tabs: [],
		selectedTabId: MAIN_TAB_ID,
		rfqs: []
	}

	componentWillMount() {
		const { props } = this
		const { api } = props

		api
			.get(this.buildURL('/list'), { username })
			.then(rfqs => this.setState({ rfqs }))
			.catch(props.handleErrorDisplay)
	}

	render () {
		const { state } = this

		return <Tabs
			closeTab={this.closeTab}
			openedTab={tabId => { this.setState({ selectedTabId: tabId }) }}
			tabs={[this.renderMainTab(state.rfqs) as any, ...state.tabs]}
			selectedTabId={state.selectedTabId}
			tabLinkStyle={{ maxWidth: 250 }} />
	}

	private closeTab = (tabId?: string): void => {
		const { state } = this

		if (!tabId) tabId = state.selectedTabId

		this.setState({
			tabs: state.tabs.filter(tab => tab.props.id != tabId),
			selectedTabId: MAIN_TAB_ID
		})
	}

	private openTab = (tab: JSX.Element): void => {
		const { tabs } = this.state
		const { id } = tab.props

		this.setState({
			tabs: tabs.some(t => t.props.id === id) ? tabs : tabs.concat(tab as any),
			selectedTabId: tab.props.id
		})
	}

	private openForm = (): void => {
		this.openTab(
			<Tab id={`new_rfq`} title='New RFQ' closable={true} >
				{this.renderCreationForm()}
			</Tab>
		)
	}

	private renderMainTab = (rfqs: RFQ[]): JSX.Element => {
		if (!rfqs) return null

		return <Tab
			id={MAIN_TAB_ID}
			title='List of RFQ programs'
			closable={false} >
			{this.renderDataGrid(rfqs)}
		</Tab>
	}

	private renderDataGrid = (rfqs: RFQ[]): JSX.Element => {
		const btnAdd = {
			btns: [{
				clickAction: () => this.openForm(),
				cssClass: 'btn btn-primary btn-trans',
				iconClass: 'fa fa-fw fa-plus',
				text: 'Add',
				disabled: false
			}]
		}

		return <div>
			<div className="bottom-margin">
				<ButtonsBar btnGroups={[btnAdd]} />
			</div>

			<DataGrid
				dataGridId="rfq_datagrid"
				columnHeaders={getColumnHeaders()}
				dataLines={renderDataLines(rfqs)}
				noItemsMsg='No RFQ programs' />
		</div>
	}

	private renderCreationForm = (): JSX.Element => {
		const today = formatDate()

		const CreationForm = forms.reduxForm({
			form: `new_rfq`,
			initialValues: { startDate: today , expiryDate: today },
			destroyOnUnmount: false,
			enableReinitialize: true
		})(CreationFormContent)

		return <CreationForm onSubmit={this.createRFQ} />
	}

	private createRFQ = (rfq): void => {
		const { props } = this

		props.api
				.post(this.buildURL('/submit'), { ...rfq, username } )
			.then(response => {
				this.setState({ rfqs: this.state.rfqs.concat(response) })
				props.displayNotification({ title: 'Info', message: 'New RFQ created', level: 'info' })
			})
			.catch(this.props.handleErrorDisplay)

		this.closeTab()
	}

	private buildURL = (path: string) => this.props.api
		.endpoints.getUrlOfFeature('arteseum.rfq', path, 'http://docker.for.mac.localhost:8000')

}

function renderDataLines (rfqs: RFQ[]): JSX.Element[] {
	if (!rfqs) return []

	return rfqs.map((rfq, idx) => {
		const cells = [
			{
				columnId: 'name',
				cssClass: 'dg-item-data-value',
				displayValue: rfq.name
			},
			{
				columnId: 'limit',
				cssClass: 'dg-item-data-value',
				displayValue: rfq.limit
			},
			{
				columnId: 'startDate',
				cssClass: 'dg-item-data-value',
				displayValue: formatDate(rfq.startDate)
			},
			{
				columnId: 'expiryDate',
				cssClass: 'dg-item-data-value',
				displayValue: formatDate(rfq.expiryDate)
			},
			{
				columnId: 'interest',
				cssClass: 'dg-item-data-value',
				displayValue: rfq.interest
			},
			{
				columnId: 'fee',
				cssClass: 'dg-item-data-value',
				displayValue: rfq.fee
			},
			{
				columnId: 'overdue',
				cssClass: 'dg-item-data-value',
				displayValue: rfq.overdue
			}
		]

		return <DataLine key={idx} cells={cells} />
	})
}

function getColumnHeaders (): Interfaces.ColumnHeader[] {
	return [
		{ id: 'name', label: 'Name', width: 200 },
		{ id: 'limit', label: 'Limit' },
		{ id: 'startDate', label: 'Start date' },
		{ id: 'expiryDate', label: 'Expiry date' },
		{ id: 'interest', label: 'Interest' },
		{ id: 'fee', label: 'Fee' },
		{ id: 'overdue', label: 'Overdue' }
	]
}
export default reactRedux.connect()(ServiceConfiguration)
