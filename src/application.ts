import { BusConnection, Constants as ServiceConstants } from '@amalto/platform6-client'

const bodyParser = require('body-parser')

import * as express from 'express'
import * as fs from 'fs'

import { Constants } from './constants'

const { PATH } = Constants

export function configure (app: express.Express) {
	app.use(bodyParser.json())

	return app
		.get(`${PATH}/portal`, function (request: express.Request, response: express.Response) {
			// Display the portal

			fs
				.createReadStream('client/build/ServiceConfiguration.bundle.js')
				.pipe(response.status(200))
		})
		.get(`${PATH}/list`, async function (request: express.Request, response: express.Response) {
			// List the existing programs

			const scriptResponse = await app.locals.service.callService({
				username: request.query.username,
				receiverId: ServiceConstants.SERVICE_SCRIPTS_ID,
				action: 'execute',
				headers: [['id', 'ListPrograms']]
			})

			const programs = BusConnection.getHeaderValue(scriptResponse, 'programs')

			response.status(200).send(programs)
		})
		.post(`${PATH}/submit`, function (request: express.Request, response: express.Response) {
			// Submit a new RFQ program
			const body = request.body

			app.locals.service.callService({
				username: body.username,
				receiverId: ServiceConstants.SERVICE_SCRIPTS_ID,
				action: 'execute',
				headers: [
					['id', 'CreateMessageInfo'],
					['name', body.name],
					['limit', body.limit],
					['startDate', body.startDate],
					['expiryDate', body.expiryDate],
					['interest', body.interest],
					['fee', body.fee],
					['overdue', body.overdue],
					['suppliers', body.suppliers.join(', ')],
					['lenders', body.lenders.join(', ')],
					['lsps', body.lsps.join(', ')],
				]
			})

			response.status(200).send(body)
		})
}
